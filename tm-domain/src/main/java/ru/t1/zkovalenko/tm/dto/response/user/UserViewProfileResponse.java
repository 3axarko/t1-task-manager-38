package ru.t1.zkovalenko.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.User;

@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable final User user) {
        super(user);
    }

}
