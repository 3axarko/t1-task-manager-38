package ru.t1.zkovalenko.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull final String message) {
        super("Error! " + message);
    }

    public AbstractException(@NotNull final String message, @NotNull final Throwable cause) {
        super("Error! " + message, cause);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super("Error! " + message, cause, enableSuppression, writableStackTrace);
    }

}