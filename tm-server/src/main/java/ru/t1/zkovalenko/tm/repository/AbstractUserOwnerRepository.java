package ru.t1.zkovalenko.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel>
        extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    public AbstractUserOwnerRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model);

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? order by %s",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                getOrderByField());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return fetch(rowSet);
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?",
                getTableName(),
                TableConstant.FIELD_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? order by %s",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                getOrderByField(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return fetch(rowSet);
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final List<M> result;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? and %s = ? order by %s LIMIT 1",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_ID,
                getOrderByField());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            result = fetch(rowSet);
        }
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @NotNull final List<M> result;
        @NotNull final String sql = String.format("SELECT * from (SELECT row_number() " +
                        "OVER () as rnum, * FROM %s WHERE %s = ?) t1 WHERE rnum=?",
                getTableName(),
                TableConstant.FIELD_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            result = fetch(rowSet);
        }
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s WHERE %s = ?",
                getTableName(),
                TableConstant.FIELD_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.getInt(1);
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

}
