package ru.t1.zkovalenko.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @Getter
    @NotNull
    private final String tableName = TableConstant.TABLE_PROJECT;

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @SneakyThrows
    public List<Project> fetch(@NotNull final ResultSet row) {
        @NotNull List<Project> projects = new ArrayList<>();
        while (row.next()) {
            @NotNull final Project project = new Project();
            project.setId(row.getString(TableConstant.FIELD_ID));
            project.setCreated(row.getTimestamp(TableConstant.FIELD_CREATED));
            project.setUserId(row.getString(TableConstant.FIELD_USER_ID));
            project.setName(row.getString(TableConstant.FIELD_NAME));
            project.setDescription(row.getString(TableConstant.FIELD_DESCRIPTION));
            @Nullable final Status status = Status.toStatus(row.getString(TableConstant.FIELD_STATUS));
            project.setStatus(status == null ? Status.NOT_STARTED : status);
            projects.add(project);
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(),
                TableConstant.FIELD_ID,
                TableConstant.FIELD_CREATED,
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_NAME,
                TableConstant.FIELD_DESCRIPTION,
                TableConstant.FIELD_STATUS);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getUserId());
            statement.setString(4, project.getName());
            statement.setString(5, project.getDescription());
            statement.setString(6, Status.NOT_STARTED.name());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? where %s = ?",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_NAME,
                TableConstant.FIELD_DESCRIPTION,
                TableConstant.FIELD_STATUS,
                TableConstant.FIELD_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getUserId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().name());
            statement.setString(5, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId,
                          @NotNull final String name,
                          @Nullable final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description == null ? "" : description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
