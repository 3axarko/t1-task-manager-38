package ru.t1.zkovalenko.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

    @Getter
    @NotNull
    private final String tableName = TableConstant.TABLE_SESSION;

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected List<Session> fetch(@NotNull final ResultSet row) {
        @NotNull List<Session> sessions = new ArrayList<>();
        while (row.next()) {
            @NotNull final Session session = new Session();
            session.setId(row.getString(TableConstant.FIELD_ID));
            session.setUserId(row.getString(TableConstant.FIELD_USER_ID));
            session.setDate(row.getTimestamp(TableConstant.FIELD_CREATED));
            @Nullable final Role role = Role.toRole(row.getString(TableConstant.FIELD_ROLE));
            session.setRole(role == null ? Role.USUAL : role);
            sessions.add(session);
        }
        return sessions;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                getTableName(),
                TableConstant.FIELD_ID,
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_CREATED,
                TableConstant.FIELD_ROLE);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setTimestamp(3, new Timestamp(session.getDate().getTime()));
            statement.setString(4, session.getRole().name());
            statement.executeUpdate();
        }
        return session;
    }

    @Override
    public @NotNull Session add(@NotNull final String userId, @NotNull final Session model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? where id = ?",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_CREATED,
                TableConstant.FIELD_ROLE,
                TableConstant.FIELD_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getUserId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getRole().name());
            statement.setString(5, session.getId());
            statement.executeUpdate();
        }
        return session;
    }

}
