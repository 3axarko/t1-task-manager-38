package ru.t1.zkovalenko.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.IRepository;
import ru.t1.zkovalenko.tm.comparator.CreatedComparator;
import ru.t1.zkovalenko.tm.comparator.NameComparator;
import ru.t1.zkovalenko.tm.comparator.StatusComparator;
import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract List<M> fetch(@NotNull final ResultSet row);

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected final String getOrderByField(@Nullable final Comparator comparator) {
        if (CreatedComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_CREATED;
        if (StatusComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_STATUS;
        if (NameComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_NAME;
        else return TableConstant.FIELD_ID;
    }

    @NotNull
    protected final String getOrderByField() {
        return getOrderByField(null);
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public abstract M update(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result;
        @NotNull final String sql = String.format("SELECT * FROM %s order by %s", getTableName(), getOrderByField());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull ResultSet resultSet = statement.executeQuery(sql);
            result = fetch(resultSet);
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final List<M> result;
        @NotNull final String sql = String.format("SELECT * FROM %s order by %s;",
                getTableName(),
                getOrderByField(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            result = fetch(resultSet);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @Nullable final List<M> result;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? order by %s LIMIT 1",
                getTableName(),
                TableConstant.FIELD_ID,
                getOrderByField());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            result = fetch(rowSet);
        }
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final Integer index) {
        @Nullable final List<M> result;
        @NotNull final String sql = String.format("SELECT * from (SELECT row_number() " +
                        "OVER (order by %s) as rnum, * FROM %s) t1 WHERE rnum = ?",
                getOrderByField(),
                getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            result = fetch(rowSet);
        }
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?",
                getTableName(),
                TableConstant.FIELD_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            return rowSet.getInt(1);
        }
    }

}
